/* const square = function (x) {
    return x*x;
};

const squareArrow = (x) => {
    return x*x;
}

const squareArrow2 = (x) => x*x;

console.log(square(8), squareArrow(8), squareArrow2(8)); */

//getFirstName
// create regular arrowFunction
// create a second arrowfunction using expression

const getFirstName = (fullName) => {
    return fullName.split(' ')[0];
};

const getFirstName2 = (fullName) => fullName.split(' ')[0];

console.log(getFirstName("Elias Medina"));
console.log(getFirstName2("Natalia Di Sarli"));