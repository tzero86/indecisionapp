// arguments object - no longer bound with arrow function

const add = (a,b) => {
    //console.log(arguments);
    return a+b;
};
console.log(add(55,1,99));

// this keyword - no longer bound with arrow function.

const user = {
    name: "Elias",
    cities: ["Chascomus", "La Plata", "Buenos Aires"],
    printPlacesLived() {
        return this.cities.map((city) => this.name + " has lived in " + city);
    }
};

console.log(user.printPlacesLived());

// CHALLENGE AREA

const multiplier = {
    numbers: [1,4,6,12],
    multiplyBy: 2,
    multiply() {
        return this.numbers.map((number) => this.multiplyBy * number );
    }
};

console.log(multiplier.multiply());