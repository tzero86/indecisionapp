class VisibilityToggle extends React.Component {
    constructor(props){
        super(props);
        this.toggleButton = this.toggleButton.bind(this);
        this.state = {
            detailsDisplayed: false
        }
    }
    toggleButton(){
        this.setState((prevState)=>{
            return {
                detailsDisplayed: !prevState.detailsDisplayed
            };
        });
    }
    render(){
        return (
            <div>
            <h1>Visibility Toggle</h1>
            <button onClick={this.toggleButton}>{this.state.detailsDisplayed ? "Hide Details" : "Show Details"}</button>
            {this.state.detailsDisplayed && 
                <div>
                <p>Hey! there are some hidden fun facts for you. Hope you like them.</p>
                <p>Scientists say that the best time to take a nap is between 1 p.m. and 2:30 p.m. 
                    because that's when a dip in body temperature makes us feel sleepy.
                </p>
            </div>
            }
        </div>
        );
    }
}

ReactDOM.render(<VisibilityToggle/>, document.getElementById("app"));

/* let detailsDisplayed = false;
const build = {
    title: "Visibility Toggle",
    details: "Hey! there are some hidden details for you",
};

const toggleButton = () => {
    detailsDisplayed = !detailsDisplayed;
    renderApp();
};


const appLocation = document.getElementById("app");
const renderApp = () => {
    const toggleTemplate = (
        <div>
            <h1>{build.title}</h1>
            <button onClick={toggleButton}>{detailsDisplayed ? "Hide Details" : "Show Details"}</button>
            {detailsDisplayed && 
                <div>
                <p>{build.details}</p>
            </div>
            }
        </div>
    );
    ReactDOM.render(toggleTemplate, appLocation);
};
renderApp(); */