import React from 'react'
  
class AboutInformation extends React.Component {
    constructor(props) {
      super(props);
      this.state = {  
        title: "React Shit",
        description: "My first react application ever.",
        author: "@tzero86"
      }
    }
    render() { 
      return ( 
          <div>
            <h1>{this.state.title}</h1>
            <h3>{this.state.description}</h3>
            <h4>{this.state.author}</h4>
          </div>
       )
    }
};

export default AboutInformation;